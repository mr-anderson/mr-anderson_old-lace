FROM nginx

RUN mkdir --parents /var/cache/nginx

ADD nginx.conf /etc/nginx/nginx.conf
ADD nginx.include.production.conf /etc/nginx/nginx.include.conf
