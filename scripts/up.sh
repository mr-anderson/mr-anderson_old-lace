#!/usr/bin/env bash

cd .. && \
docker-compose -f docker-compose.yml -f docker-compose.development.yml up --build -d \
    && docker-compose logs -f
